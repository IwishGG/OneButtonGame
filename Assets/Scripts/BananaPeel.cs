using System.Collections;
using UnityEngine;

public class BananaPeel : MonoBehaviour
{
    [SerializeField]
    private float _flipForce;
    [SerializeField]
    private float _flipTime = 1f;
    [SerializeField]
    private AudioSource _flipSound;

    private Rigidbody2D _rb;

    void OnTriggerEnter2D(Collider2D other)
    {
        _rb = other.GetComponent<Rigidbody2D>();

        if (other.GetComponent<Stilts>() || other.GetComponent<Ball>())
        {
            GameManager.IsBanana = true;
            GameManager.CanControlPlayer = false;
            _rb.velocity += new Vector2(_flipForce, 0f);
            StartCoroutine(RecoverControl(_flipTime));
            _flipSound.Play();
        }
    }

    IEnumerator RecoverControl(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject, _flipTime / 20f);
        GameManager.CanControlPlayer = true;
        GameManager.IsBanana = false;

    }

}
