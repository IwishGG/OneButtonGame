using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spark : MonoBehaviour
{

    [SerializeField]
    private AudioSource _spark;
    void OnCollisionEnter2D(Collision2D other)
    {
        var colRb = other.gameObject.GetComponent<Rigidbody2D>();

        if (other.gameObject.GetComponent<Ball>() || gameObject.GetComponent<Stilts>())
        {
            if (_spark.isPlaying == false)
            {
                _spark.Play();
                GameManager.IsSpark = true;

            }
            GameManager.CanControlPlayer = false;
            colRb.gravityScale = -0.5f;
            StartCoroutine(LoadScene(2f));

        }
    }
    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        GameManager.CanControlPlayer = true;
        if (GameManager.GameWin == false)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
