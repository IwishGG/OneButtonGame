using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StiltsController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D _leftStilt;
    [SerializeField]
    private Rigidbody2D _rightStilt;
    [SerializeField]
    private Rigidbody2D _mainBody;
    [SerializeField]
    private CircleCollider2D _headCol;
    [SerializeField]
    private LayerMask _groundLayer;
    [SerializeField]
    private Camera _camera;

    [SerializeField]
    private LeftCheck _leftCheck;
    [SerializeField]
    private RightCheck _rightCheck;

    [SerializeField]
    private float _rotateSpeed = 10f;
    [SerializeField]
    private float _moveSpeed = 1f;
    [SerializeField]
    private float _moveUpSpeed = 0.8f;
    [SerializeField]
    private float _fallingHeight = 2f;
    [SerializeField]
    private Vector2 _moveDistance;

    private bool _isChanging;

    private Vector3 _mousePos;
    private Vector3 _difference;
    private float _rotationLeft;
    private float _rotationRight;

    private Vector2 _positionLeft;
    private Vector2 _positionRight;

    void Start()
    {
        _isChanging = false;
        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        transform.position = CheckPoint.Gm.lastCheckPointPos;
    }

    // Update is called once per frame
    void Update()
    {
        GetTransform();
        if (IsFalling())
        {
            GameManager.CanControlPlayer = false;
            StartCoroutine(LoadScene(2f));
        }

        if (_leftCheck.IsGroundedL() || _rightCheck.IsGroundedR())
        {
            if (_isChanging == true && GameManager.CanControlPlayer)
            {
                MovingLeftStilt();
            }
            else if (_isChanging == false && GameManager.CanControlPlayer)
            {
                MovingRightStilt();
            }
        }
        else if (_leftCheck.IsGroundedL() == false && _rightCheck.IsGroundedR() == false)
        {
            return;

        }



    }

    private void MovingLeftStilt()
    {

        if (Input.GetMouseButton(0))
        {
            _leftStilt.MoveRotation(Mathf.LerpAngle(_leftStilt.rotation, _rotationLeft / 1.5f, _rotateSpeed * Time.fixedDeltaTime));


            if (_leftStilt.position.y <= _positionRight.y && _leftStilt.position.x <= _positionRight.x)
            {
                _leftStilt.velocity = new Vector3(_moveSpeed * 1.2f, _moveUpSpeed, 0f);
            }
            else if (_leftStilt.position.y > _positionLeft.y && _leftStilt.position.x > _positionRight.x)
            {
                _leftStilt.velocity = new Vector3(0f, 0f, 0f);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _isChanging = false;

        }
        return;
    }
    private void MovingRightStilt()
    {
        if (Input.GetMouseButton(0))
        {
            _rightStilt.MoveRotation(Mathf.LerpAngle(_rightStilt.rotation, _rotationRight, _rotateSpeed * Time.fixedDeltaTime));


            if (_rightStilt.position.y <= _positionRight.y && _rightStilt.position.x <= _positionRight.x)
            {
                _rightStilt.velocity = new Vector3(_moveSpeed, _moveUpSpeed, 0f);
            }

            else if (_rightStilt.position.y > _positionRight.y && _rightStilt.position.x > _positionRight.x)
            {
                _rightStilt.velocity = new Vector3(0f, 0f, 0f);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _isChanging = true;

        }
        return;
    }

    private void GetTransform()
    {

        if (Input.GetMouseButtonDown(0))
        {
            _positionLeft = _leftStilt.position + new Vector2(_moveDistance.x, _moveDistance.y / 1.5f);
            _positionRight = _rightStilt.position + _moveDistance;
        }
        _mousePos = new Vector3(_camera.ScreenToViewportPoint(Input.mousePosition).x, _camera.ScreenToWorldPoint(Input.mousePosition).y, 0);
        _difference = _mousePos - transform.position;
        _rotationLeft = Mathf.Atan2(_positionLeft.x, -_positionRight.y) * Mathf.Rad2Deg;
        _rotationRight = Mathf.Atan2(_positionRight.x, -_positionRight.y) * Mathf.Rad2Deg;

    }

    private bool IsFalling()
    {

        var fallRaycastHit = Physics2D.BoxCast(_headCol.bounds.center, _headCol.bounds.size / 2, 0f, Vector2.down, _fallingHeight, _groundLayer);
        return fallRaycastHit.collider != null;

    }
    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        if (GameManager.GameWin == false)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }


    }


}
