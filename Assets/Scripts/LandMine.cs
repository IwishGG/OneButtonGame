using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LandMine : MonoBehaviour
{
    private Collider2D[] _inExplosionRadius = null;
    [SerializeField]
    private float _forceMuilt = 5f;
    [SerializeField]
    private float _radius = 5f;
    [SerializeField]
    private AudioSource _boom;
    [SerializeField]
    private GameObject _explosion;
    [SerializeField]
    private GameObject _light;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.GetComponent<Stilts>() || other.collider.GetComponent<Ball>() || other.collider.GetComponent<PlayerBody>())
        {
            StartCoroutine(LoadScene(1.8f));
            Explode();
            GameManager.CanControlPlayer = false;
            _light.SetActive(false);

        }
    }

    private void Explode()
    {

        _inExplosionRadius = Physics2D.OverlapCircleAll(transform.position, _radius);

        foreach (Collider2D item in _inExplosionRadius)
        {
            Rigidbody2D itemRb = item.GetComponent<Rigidbody2D>();
            if (itemRb != null)
            {
                Vector2 distance = (item.transform.position - transform.position) / 2f;
                if (distance.magnitude > 0)
                {

                    float explosionForce = _forceMuilt / distance.magnitude;
                    itemRb.AddForce(distance.normalized * explosionForce);
                    if (_boom.isPlaying == false)
                    {
                        _boom.Play();
                        Instantiate(_explosion, gameObject.transform.position, gameObject.transform.rotation);
                    }

                }
            }
        }
    }

    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        GameManager.CanControlPlayer = true;
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _radius);
    }
}
