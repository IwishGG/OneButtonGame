using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField]
    private AudioSource _winSound;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Ball>() || other.gameObject.GetComponent<PlayerBody>())
        {
            if (GameManager.CanControlPlayer)
            {
                GameManager.CanControlPlayer = false;
                GameManager.GameWin = true;
                _winSound.Play();
            }
        }
    }
}
