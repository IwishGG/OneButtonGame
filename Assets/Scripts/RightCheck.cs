using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RightCheck : MonoBehaviour
{
    [SerializeField]
    private LayerMask _groundLayer;
    private BoxCollider2D _col;

    [SerializeField]
    private AudioSource _rightStep;
    // Start is called before the first frame update
    void Start()
    {
        _col​​​​​​​​​​​​​​​​​​​ = GetComponent<BoxCollider2D​​​​​​​​​​​​​​​​​​​>();

    }

    public bool IsGroundedR()
    {
        var _fallingHeight = 0.1f;
        var fallRaycastHit = Physics2D.BoxCast(_col.bounds.center, _col.bounds.size, 0f, Vector2.down, _fallingHeight, _groundLayer);
        return fallRaycastHit.collider != null;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        Vector3 offset = transform.position - other.transform.position;

        if (offset.sqrMagnitude > 0.5f && !other.GetComponent<Stilts>() && GameManager.CanControlPlayer == true)
        {
            if (_rightStep.isPlaying == false)
            {
                _rightStep.Play();
            

            }
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        Vector3 offset = transform.position - other.transform.position;

        if (offset.sqrMagnitude > 0.5f && !other.GetComponent<Stilts>() && GameManager.CanControlPlayer == true)
        {
            if (_rightStep.isPlaying)
            {
                _rightStep.Stop();
            }
        }

    }
}
