using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIController : MonoBehaviour
{

    [SerializeField]
    private GameObject _levelChoose;

    // Update is called once per frame
    void Update()
    {

    }

    public void LevelChoose()
    {
        _levelChoose.gameObject.SetActive(true);

    }
    public void BackMainMenu()
    {
        _levelChoose.gameObject.SetActive(false);

    }
    public void Stilts()
    {
        SceneManager.LoadScene("Stilts");


    }
    public void Wobble()
    {

        SceneManager.LoadScene("Wobble");

    }
    public void Quit()
    {
        Application.Quit();
    }


}
