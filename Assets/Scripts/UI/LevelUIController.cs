using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject _winUI;
    [SerializeField]
    private GameObject _pausedUI;

    private GameManager _gm;

    private void Start()
    {
        Time.timeScale = 1f;
        _gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameWin == true)
        {
            ShowGameWinUI();
        }
        else
        {
            _winUI.SetActive(false);
        }

    }

    public void ShowGameWinUI()
    {
        _winUI.SetActive(true);
    }

    public void PlayAgain()
    {
        _pausedUI.SetActive(false);
        _winUI.SetActive(false);
        Time.timeScale = 1f;
        GameManager.IsPaused = false;
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    public void Resume()
    {
        _pausedUI.SetActive(false);
        Time.timeScale = 1f;
        GameManager.IsPaused = false;
    }
    public void Paused()
    {
        _pausedUI.SetActive(true);
        Time.timeScale = 0f;
        GameManager.IsPaused = true;
    }
    public void BackToMainMenu()
    {
        _pausedUI.SetActive(false);
        _winUI.SetActive(false);
        Time.timeScale = 1f;
        _gm.lastCheckPointPos = new Vector2(0, 0);
        GameManager.IsPaused = false;
        SceneManager.LoadScene("MainMenu");

    }
}
