using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SmokeFXHead : MonoBehaviour
{
    [SerializeField]
    private GameObject _smokeTrail;
    private Rigidbody2D _rigidBody;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (GameManager.CanControlPlayer == true || GameManager.IsBanana == true ||GameManager.IsSpark==true)
        {
            _smokeTrail.SetActive(false);
        }
        if ( GameManager.IsSpark==false && GameManager.CanControlPlayer == false )
        {
            Vector2 _stopVel = new Vector2(0f, 0f);
            if (_rigidBody.velocity != _stopVel)
            {
                _smokeTrail.SetActive(true);
            }
        }

    }
}
