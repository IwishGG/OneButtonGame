using DG.Tweening;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField]
    private Transform _startPos;
    //final position object will move
    [SerializeField]
    private Transform _finalPos;
    //ease type to decide the movement
    [SerializeField]
    private Ease _ease;
    //ease time movement
    [SerializeField]
    private float _durationTimer;
    [SerializeField]
    private GameObject _player;



    private Vector3 currentTargetPos;
    // Start is called before the first frame update
    void Start()
    {

        DOTween.Restart(transform);
        //set originalPos
        transform.position = _startPos.position;
        //set next target position
        currentTargetPos = _finalPos.position;
        if (transform != null)
        {
            //move object
            MoveObj(currentTargetPos);
        }
    }

    void MoveObj(Vector3 pos)
    {
        //move object to target position in give time
        var t = transform.DOMove(pos, _durationTimer).SetEase(_ease).OnComplete(() => MoveObj(currentTargetPos)).SetDelay(0.5f);
        //set the target position
        currentTargetPos = currentTargetPos == _finalPos.position ? transform.position : _finalPos.position;
        t.SetAutoKill(false);
    }

    private void OnTriggerStay2D(Collider2D other)
    {

        other.transform.root.gameObject.transform.parent = transform;


    }
    private void OnTriggerExit2D(Collider2D other)
    {

        _player.transform.parent = null;


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(_startPos.position, 0.5f);
        Gizmos.DrawLine(_startPos.position, _finalPos.position);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_finalPos.position, 0.5f);
    }

    private void OnDisable()
    {
        DOTween.Pause(transform);
    }

}
