using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class LeftCheck : MonoBehaviour
{
    [SerializeField]
    private LayerMask _groundLayer;
    [SerializeField]
    private AudioSource _leftStep;

    private BoxCollider2D _col;
    // Start is called before the first frame update
    void Start()
    {
        _col​​​​​​​​​​​​​​​​​​​ = GetComponent<BoxCollider2D​​​​​​​​​​​​​​​​​​​>();

    }

    public bool IsGroundedL()
    {
        var _fallingHeight = 0.1f;
        var fallRaycastHit = Physics2D.BoxCast(_col.bounds.center, _col.bounds.size, 0f, Vector2.down, _fallingHeight, _groundLayer);
        return fallRaycastHit.collider != null;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Vector3 offset = transform.position - other.transform.position;

        if (offset.sqrMagnitude > 0.1f && !other.GetComponent<Stilts>() && GameManager.CanControlPlayer == true)
        {
            if (_leftStep.isPlaying == false)
            {
                _leftStep.Play();
            }
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        Vector3 offset = transform.position - other.transform.position;

        if (offset.sqrMagnitude > 0.5f && !other.GetComponent<Stilts>() && GameManager.CanControlPlayer == true)
        {
            if (_leftStep.isPlaying)
            {
                _leftStep.Stop();
                Debug.Log("456");

            }
        }

    }
}
