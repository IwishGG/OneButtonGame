using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool CanControlPlayer;
    public static bool GameWin;
    public static bool IsPaused;
    public static bool IsGameScene;
    public static bool IsBanana;
    public static bool IsSpark;

    private static GameManager instance;
    public Vector2 lastCheckPointPos;

    void Awake()
    {
        CanControlPlayer = true;
        GameWin = false;
        IsPaused = false;
        IsBanana = false;
        IsSpark = false;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    // Update is called once per frame
    void Update()
    {

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "MainMenu")
        {
            IsGameScene = false;
        }
        if (scene.name == "Wobble")
        {
            IsGameScene = true;

        }
        if (scene.name == "Stilts")
        {
            IsGameScene = true;
        }
    }

    public void LoadStilts()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    public void LoadWobble()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }


}
