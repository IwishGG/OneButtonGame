using UnityEngine;

public class SmokeFX : MonoBehaviour
{
    [SerializeField]
    private GameObject _smokeTrail;


    void Update()
    {
        if (GameManager.CanControlPlayer == true)
        {
            _smokeTrail.SetActive(false);
        }
        else if (GameManager.CanControlPlayer == false)
        {
             _smokeTrail.SetActive(true);
        }

    }
}
