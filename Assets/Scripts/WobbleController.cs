using UnityEngine;
using UnityEngine.UI;


public class WobbleController : MonoBehaviour
{
    [SerializeField]
    private Slider _jumpUI;
    [SerializeField]
    private LayerMask _jumpableLayer;
    [SerializeField]
    private GameObject _ball;
    [SerializeField]
    private Rigidbody2D _rbBall;
    [SerializeField]
    private float _jumpForce = 1f;
    [SerializeField]
    private float _jumpChargingRate = 5f;
    [SerializeField]
    private float _maxJumpPressure = 5f;

    [Header("Balance")]
    [SerializeField]
    private float _targetRotation;
    [SerializeField]
    private float _targetMaxRotation;
    [SerializeField]
    private float _force;

    [Header("sfx & fx")]
    [SerializeField]
    private AudioSource _jump;

    private float _jumpPressure = 0f;
    private CircleCollider2D _col;
    private float uiValue;
    private float timerJump;
    private float timerBalance;

    void Start()
    {
        _col = _ball.GetComponent<CircleCollider2D>();
        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        transform.position = CheckPoint.Gm.lastCheckPointPos;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) == true && IsGrounded() && GameManager.CanControlPlayer == true)
        {
            timerBalance += Time.deltaTime;
            float randNum = Mathf.Sin(timerBalance * _force) * _targetMaxRotation;
            _rbBall.MoveRotation(Mathf.LerpAngle(_rbBall.rotation, randNum + _targetRotation, _force * Time.fixedDeltaTime));
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) == true)
        {
            timerBalance = 0f;
        }

        if (IsGrounded() && GameManager.CanControlPlayer == true)
        {
            BallJump();
            _jumpUI.value = uiValue;
        }
        else
        {
            if (GameManager.IsBanana)
            {
                _jumpPressure = 0f;
                timerJump = 0f;
            }
        }
    }
    private void BallJump()
    {

        if (Input.GetMouseButton(0))
        {

            // if (_jumpPressure < _maxJumpPressure)
            // {
            //_jumpPressure += Time.deltaTime * _maxJumpPressure;
            timerJump += Time.deltaTime;
            _jumpPressure = Mathf.Sin(timerJump / _jumpChargingRate * _maxJumpPressure);
            _jumpPressure = Mathf.Abs(_jumpPressure);
            _jumpPressure = _jumpPressure * _maxJumpPressure;
            // }
            //  else
            //  {
            //   _jumpPressure = _maxJumpPressure;


            // }
        }
        else if (_jumpPressure > 0f)
        {

            var jumpVel = _jumpPressure * _jumpForce + 1f;
            _rbBall.velocity = new Vector3(jumpVel / 5f, jumpVel, 0f);
            _jumpPressure = 0f;
            timerJump = 0f;

            float _randNum = Random.Range(1f, 1.2f);
            _jump.pitch = _randNum;
            _jump.Play();
        }
        uiValue = _jumpPressure / _maxJumpPressure;


    }

    private bool IsGrounded()
    {
        float extraHeifht = 0.1f;
        var raycastHit = Physics2D.BoxCast(_col.bounds.center, _col.bounds.size, 0f, Vector2.down, extraHeifht, _jumpableLayer);
        Color rayColor;
        if (raycastHit.collider != null)
        {
            rayColor = Color.green;

        }
        else
        {
            rayColor = Color.red;

        }
        Debug.DrawRay(_col.bounds.center + new Vector3(_col.bounds.extents.x, 0f), Vector2.down * (_col.bounds.extents.y + extraHeifht));

        return raycastHit.collider != null;
    }
}
