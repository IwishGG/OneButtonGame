using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseControlFX : MonoBehaviour
{

    [SerializeField]
    private ParticleSystem _loseControlFX;

    void Update()
    {
        if (GameManager.CanControlPlayer == true)
        {
            _loseControlFX.Stop();
        }
        if (GameManager.CanControlPlayer == false && GameManager.IsSpark == true || GameManager.IsBanana == true)
        {
            ParticleSystem ps = _loseControlFX.GetComponent<ParticleSystem>();
            ParticleSystem.EmitParams emitOverride = new ParticleSystem.EmitParams();
            emitOverride.startLifetime = 1f;
            ps.Emit(emitOverride, 1);
        }

    }
}
