using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioSource _bgSource;
    public static AudioSource _aBgSource;



    [SerializeField]
    private AudioSource _aBgSourceLoader;
    [SerializeField]
    private AudioSource _bgSourceLoader;
    [SerializeField]
    private AudioSource _ClickSource;


    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        
        _aBgSource = _aBgSourceLoader;
        _bgSource = _bgSourceLoader;

       
    }
    public static AudioSource GetBGSource()
    {
        return _bgSource;
    }

    public static AudioSource GetABGSource()
    {
        return _aBgSource;
    }
    public void PlayClickSource()
    {
        _ClickSource.Play();
    }


}
