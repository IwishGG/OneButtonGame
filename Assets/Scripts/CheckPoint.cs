using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public static GameManager Gm;

    [SerializeField]
    private AudioSource _sound;
    private Vector2 _pos;
    private void Start()
    {
        Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        _pos = new Vector2(transform.position.x, transform.position.y);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Ball>() || other.gameObject.GetComponent<PlayerBody>())
        {
            if (GameManager.CanControlPlayer)
            {
                if (Gm.lastCheckPointPos != _pos)
                {
                    Gm.lastCheckPointPos = transform.position;
                    _sound.Play();
                    Debug.Log("Checked");
                }

            }

        }
    }
}
